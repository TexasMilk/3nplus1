# 3nPlus1

Testing a user defined set of numbers against the Collatz conjecture, up to 1 million steps per number

# Todo

1. Add user defined range
2. Error detecting and handling
3. User defined output from command line
4. Move Tree code into own file
5. further optimization
