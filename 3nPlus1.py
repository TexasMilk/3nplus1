import math
import sys

#all tree code shamelessly stolen from the internet
class Node:

    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data

    def insert(self, data):
        if self.data:
            if data < self.data:
                if self.left is None:
                    self.left = Node(data)
                else:
                    self.left.insert(data)
            elif data > self.data:
                if self.right is None:
                    self.right = Node (data)
                else:
                    self.right.insert(data)
        else:
            self.data = data

    def search(self, key):
        if key < self.data:
            if self.left is None:
                return -1
            return self.left.search(key)
        elif key > self.data:
            if self.right is None:
                return -1
            return self.right.search(key)
        else:
            self.data = key

    def PrintTree(self):
        if self.left:
            self.left.PrintTree()
        print(self.data)
        if self.right:
            self.right.PrintTree()

#my work starts here!
class Sim:

    def __init__(self):
        #number the sim will run until
        self.max = 10000
        #whether or not to print tree after execution
        self.printTree = True
        #whether or not to print debug info
        self.debug = False
        #whether or not to print all operations
        self.printOps = True
        #number the sim will exit at if it performs that many operations on a single number before reaching 1
        self.maxCounter = 1000000000
        #number to start with
        self.start = 2

def commandLineHandler(sim: Sim):
    if(len(sys.argv) != 6)
        print("""Error: not enough args
            Usage: 3nPlus1.py <max> <printTree> <debug> <printOps> <maxCounter> <start>""")
    if(isinstance(sys.argv[0], int))
        sim.max = sys.argv[0]
    
    if(isinstance(sys.argv[1], bool))
        sim.printTree = sys.argv[1]

    if(isinstance(sys.argv[2], bool))
        sim.debug = sys.argv[2]
    
    if(isinstance(sys.argv[3], bool))
        sim.printOps = sys.argv[3]

    if(isinstance(sys.argv[4], int))
        sim.maxCounter = sys.argv[1]

    if(isinstance(sys.argv[5], int))
        sim.start = sys.argv[5]

    return

def runSim(sim: Sim, tree: Node):
    #start at the starting number (default is 2)
    i = sim.start

    #while i has not reached the user defined maximum value to be tested
    while i < sim.max:
        #print what number is being tested if printing is enabled
        if(sim.printOps == True):
            print("The number being tested is %d" % i)
        #initialize temp value j to i for manipulation
        j = i
        i += 1
        #continue to manipulate j until it reaches 1 or the maxCounter is reached
        while j != 1:
            #initialzie the counter to 0
            c = 0
            #if maxCounter is reached, exit and reevalute algorithm
            if(c > sim.maxCounter):
                sys.exit("%d has reached the maximum number of steps, exiting..." % i)
            #if j is a power of 2 it will go straight to one (all even cases)
            if math.log(j, 2) % 1 == 0:
                if(sim.printOps == True):
                    print("%d is a power of 2, skipping" % j)
                j = 1
            #if the tree contains the value, skip this case
            elif tree.search(j) != -1:
                if(sim.printOps == True):
                    print("%d already found in previous case, skipping" % j)
                #skip case by terminating loop prematurely
                j = 1  
            #if j is even, divide it by 2 and iterate the counter
            elif j % 2 == 0:
                if(sim.printOps == True):
                    print("%d" % j)
                tree.insert(j)
                j = j / 2
                c += 1
            #j must be odd, multiply it by 3, add 1, then immediately divide by 2 (must be even)
            else:
                if(sim.printOps == True):
                    print("%d" % j)
                tree.insert(j)
                j = (3 * j + 1) / 2
                c += 1
     #iterate the tested value
    i += 1

    if(sim.printTree == True):
        print("\nTree starts here\n")
        tree.PrintTree()

def main():
    
    sim = Sim()

    tree = Node(1)

    commandLineHandler(sys.argv, sim)

    runSim(sim, tree)



main()